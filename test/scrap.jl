using LinearAlgebra, StaticArrays
using BenchmarkTools

using FastSymplecticIntegrators

using FastSymplecticIntegrators: Tao, step, integrate

const G = 1.0
const m = 1.0
const k = 1.0

hoH(q, p) = (p⋅p)/(2*m) + (k/2)*(q⋅q)

ho∂qH(q, p) = k*q
ho∂pH(q, p) = p/m

function invmetric(x::AbstractVector)
    A = 1 - 2*G*m/x[1]
    @SMatrix eltype(x)[
        A 0 0 0
        0 x[1]^(-2) 0 0
        0 0 (x[1]*sin(x[2]))^(-2) 0
        0 0 0 -(1/A)
    ]
end

H(x, p) = p' * invmetric(x) * p ./ 2

∂pH(x, p) = invmetric(x)*p

function ∂qH(x, p)
    r = x[1]
    A = 1 - 2*G*m/r
    ∂A = 2*G*m/r^2
    (s, c) = sincos(x[2])
    SVector{4,eltype(x)}(
        ((p[4]/A)^2*∂A*p[4]^2 + ∂A*p[1]^2 - 2*p[2]^2/r^3 - 2*p[3]^2/(r^3*s^2))/2,
        -c*p[3]^2/(r^2*s^3),
        zero(eltype(x)),
        zero(eltype(x)),
    )
end


src1() = quote
    q0 = @SVector Float64[1,1]
    p0 = @SVector Float64[0,0]

    int = Tao{2}(ho∂qH, ho∂pH)

    (q, p) = integrate(int, q0, p0, 2*π; nsteps=1)

    #s0 = FastSymplecticIntegrators.initstate(int, q0, p0)
    #@code_native step(int, s0, 0.1)
end

src2() = quote
    #TODO: these are totally arbitrary right now, just testing performance
    q0 = @SVector Float64[10, π/2, 0, 0]
    p0 = @SVector Float64[0, 0, 0, 5]

    int = Tao{2}(∂qH, ∂pH)

    (q, p) = integrate(int, q0, p0, 1.0; nsteps=100)
end

function hobench1()
    q0 = @SVector Float64[1,1]
    p0 = @SVector Float64[0,0]

    int = Tao{2}(ho∂qH, ho∂pH)

    @benchmark integrate($int, $q0, $p0, 2*π; nsteps=100)
end

function bench1()
    #TODO: these are totally arbitrary right now, just testing performance
    q0 = @SVector Float64[10, π/2, 0, 0]
    p0 = @SVector Float64[0, 0, 0, 5]

    int = Tao{2}(∂qH, ∂pH)

    @benchmark integrate($int, $q0, $p0, 1.0; nsteps=100)
end


function refnn(q0, p0, ws, bs)
    qp = vcat(q0,p0)
    o = tanh.(ws[1]*qp + bs[1])
    #FUCK: can't get this to stop allocating so I'm being lazy
    o = tanh.(ws[2]*o + bs[2])
    o = tanh.(ws[3]*o + bs[3])
    o = tanh.(ws[4]*o + bs[4])
    o = tanh.(ws[5]*o + bs[5])
    o = tanh.(ws[6]*o + bs[6])
    o
end

# the idea here is to compare to performance of an efficient NN
function refbench(l::Integer=6, w::Integer=128)
    ws = ntuple(l) do j
        if j == 1
            SMatrix{w,8}(randn(w,8))
        elseif j == l
            SMatrix{8,w}(randn(8,w))
        else
            SMatrix{w,w}(randn(w,w))
        end
    end
    bs = ntuple(l) do j
        if j == l
            SVector{8}(randn(8))
        else
            SVector{w}(randn(w))
        end
    end

    q0 = @SVector Float64[10, π/2, 0, 0]
    p0 = @SVector Float64[0, 0, 0, 5]

    #====================================================================================================
    had some trouble stopping this from allocating, so I cheated...

    note that with staticarrays compilation takes forever, this is a real problem that we'd have
    to figure out a way around in real life if we took this approach

    otherwise, takes around 50 μs... this it 2 or 3 times faster than fast tao
    quite significant, but considering complications of dealing with NN, far from clear it'd be worth it

    Also not clear if a real nn could be this narrow
    ====================================================================================================#

    @benchmark refnn($q0, $p0, $ws, $bs)
end

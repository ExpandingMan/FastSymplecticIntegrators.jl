using FastSymplecticIntegrators
using Documenter

DocMeta.setdocmeta!(FastSymplecticIntegrators, :DocTestSetup, :(using FastSymplecticIntegrators); recursive=true)

makedocs(;
    modules=[FastSymplecticIntegrators],
    authors="ExpandingMan <savastio@protonmail.com> and contributors",
    sitename="FastSymplecticIntegrators.jl",
    format=Documenter.HTML(;
        canonical="https://ExpandingMan.gitlab.io/FastSymplecticIntegrators.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)

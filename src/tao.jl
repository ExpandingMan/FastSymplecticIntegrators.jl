
struct TaoState{V<:AbstractVector} <: FastSymplecticIntegratorState
    q::V
    p::V
    x::V
    y::V

    function TaoState(q::AbstractVector, p::AbstractVector, x::AbstractVector, y::AbstractVector)
        (q, p, x, y) = promote(q, p, x, y)
        new{typeof(q)}(q, p, x, y)
    end
end

position(qpxy::TaoState) = qpxy.q
momentum(qpxy::TaoState) = qpxy.p


struct Tao{l,QH,PH,T<:Number} <: FastSymplecticIntegrator
    ∂qH::QH
    ∂pH::PH

    ω::T
end

function Tao{l}(∂qH, ∂pH; ω::Real=1.0) where {l}
    Tao{l,typeof(∂qH),typeof(∂pH),typeof(ω)}(∂qH, ∂pH, ω)
end

initstate(int::Tao, q::AbstractVector, p::AbstractVector) = TaoState(q, p, q, p)


@inline function _Astep(int::Tao, qpxy::TaoState, h::Real)
    (q, p, x, y) = (qpxy.q, qpxy.p, qpxy.x, qpxy.y)
    ∂qH = int.∂qH(q, y)
    ∂yH = int.∂pH(q, y)
    q′ = q
    p′ = p - h*∂qH
    x′ = x + h*∂yH
    y′ = y
    TaoState(q′, p′, x′, y′)
end

@inline function _Bstep(int::Tao, qpxy::TaoState, h::Real)
    (q, p, x, y) = (qpxy.q, qpxy.p, qpxy.x, qpxy.y)
    ∂pH = int.∂pH(x, p)
    ∂xH = int.∂qH(x, p)
    q′ = q + h*∂pH
    p′ = p
    x′ = x
    y′ = y - h*∂xH
    TaoState(q′, p′, x′, y′)
end

@inline function _Cstep(int::Tao, qpxy::TaoState, h::Real)
    (q, p, x, y) = (qpxy.q, qpxy.p, qpxy.x, qpxy.y)
    (s, c) = convert.((eltype(q),), sincos(2*int.ω*h))
    q′ = ((q + x) + c*(q - x) + s*(p - y))/2
    p′ = ((p + y) - s*(q - x) + c*(p - y))/2
    x′ = ((q + x) - c*(q - x) - s*(p - y))/2
    y′ = ((p + y) + s*(q - x) - c*(p - y))/2
    TaoState(q′, p′, x′, y′)
end

@inline function _step_lowest_order(int::Tao, qpxy::TaoState, h::Real)
    qpxy = _Astep(int, qpxy, h/2)
    qpxy = _Bstep(int, qpxy, h/2)
    qpxy = _Cstep(int, qpxy, h)
    qpxy = _Bstep(int, qpxy, h/2)
    qpxy = _Astep(int, qpxy, h/2)
    qpxy
end

@inline taoγ(l::Integer) = 1/(2 - 2^(1/(l + 1)))
const taoγ_1 = taoγ(1)

@inline function _step_composite(int::Tao, ::Val{1}, qpxy::TaoState, h::Real)
    h = convert(eltype(qpxy.q), h)
    γ = convert(typeof(h), taoγ_1)
    qpxy = _step_lowest_order(int, qpxy, γ*h)
    qpxy = _step_lowest_order(int, qpxy, (1 - 2*γ)*h)
    qpxy = _step_lowest_order(int, qpxy, γ*h)
    qpxy
end

@inline function _step_composite(int::Tao, ::Val{l}, qpxy::TaoState, h::Real) where {l}
    h = convert(eltype(qpxy.q), h)
    γ = convert(typeof(h), taoγ(l))
    qpxy = _step_composite(int, Val(l-1), qpxy, γ*h)
    qpxy = _step_composite(int, Val(l-1), qpxy, (1 - 2*γ)*h)
    qpxy = _step_composite(int, Val(l-1), qpxy, γ*h)
    qpxy
end

@inline step(int::Tao{l}, qpxy::TaoState, h::Real) where {l} = _step_composite(int, Val(l), qpxy, h)


module FastSymplecticIntegrators

using LinearAlgebra, StaticArrays, ConcreteStructs
using DispatchDoctor: @stable


abstract type FastSymplecticIntegrator end

abstract type FastSymplecticIntegratorState end


@stable function steps(int::FastSymplecticIntegrator, q0::AbstractVector, p0::AbstractVector, h::Real, nsteps::Integer=1)
    s = initstate(int, q0, p0)
    for j ∈ 1:nsteps
        s = step(int, s, h)
    end
    s
end
function integrate(int::FastSymplecticIntegrator, q0::AbstractVector, p0::AbstractVector, h::Real, nsteps::Integer=1)
    s = steps(int, q0, p0, h, nsteps)
    (position(s), momentum(s))
end

function integrate(int::FastSymplecticIntegrator, t::Real, q0::AbstractVector, p0::AbstractVector, h::Real)
    nsteps = Int((÷)(t, h, RoundNearest))
    integrate(int, q0, p0, h, nsteps)
end
function integrate(int::FastSymplecticIntegrator, t::Real, nsteps::Integer, q0::AbstractVector, p0::AbstractVector)
    h = t/nsteps
    integrate(int, q0, p0, h, nsteps)
end

function trajectorysteps(int::FastSymplecticIntegrator, q0::AbstractVector, p0::AbstractVector, h::Real, nsteps::Integer=1)
    s = initstate(int, q0, p0)
    o = Vector{Tuple{typeof(position(s)),typeof(momentum(s))}}(undef, n)
    o[1] = (position(s), momentum(s))
    for j ∈ 2:n
        s = step(int, s, h)
        o[j] = (position(s), momentum(s))
    end
    o
end

function trajectory(int::FastSymplecticIntegrator, t::Real, q0::AbstractVector, p0::AbstractVector, h::Real)
    nsteps = Int((÷)(t, h, RoundNearest))
    trajectorysteps(int, q0, p0, h, nsteps)
end
function trajectory(int::FastSymplecticIntegrator, t::Real, nsteps::Integer, q0::AbstractVector, p0::AbstractVector)
    h = t/nsteps
    trajectorysteps(int, q0, p0, h, nsteps)
end


include("tao.jl")


export FastSymplecticIntegrator


end

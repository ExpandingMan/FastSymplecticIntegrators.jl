# FastSymplecticIntegrators

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/FastSymplecticIntegrators.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/FastSymplecticIntegrators.jl/main?style=for-the-badge)](https://gitlab.com/ExpandingMan/FastSymplecticIntegrators.jl/-/pipelines)

A package for fast, zero allocation, preferrably non-branching explicit symplectic integrators.
This is most appropriate for "low" dimensions $n \lesssim 10$.

One of the original goals of this was to see if a neural network trained on a solution is any faster
than direct integration.  Initial testing suggests that under ideal circumstances a neural network is
only 2 to 3 times faster than the Tao 2016 algorithm for integrating Schwarzschild geodesics
($n=4$).  This is of course very rough and there are many parameters to this comparison, but it casts
some doubt for me on the usefulness of NN's here as their implementation (or rather the
implementation of their training) tends to be significantly more complicated than straight-forward
integration.

At time of writing we only implement [Tao 2016](https://arxiv.org/abs/1609.02212).  While this is a
relatively complicated algorithm, it is the only known explicit algorithm for non-separable
Hamiltonians, and therefore the most general.
